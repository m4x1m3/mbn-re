import sys;
import api;

auth_token = "";

try:
    with open("auth_token", "r") as f:
        auth_token = f.read();
        
        if (api.validate_token(auth_token)):
            print("OK, token toujours valide!");
            sys.exit(0);
        else:
            print("ERREUR, token invalide, veuillez vous reconnecter.");
except FileNotFoundError:
    pass;


user = input("USER: ");
password = input("TOKEN: ");

print("REQUESTING...");
auth_token = api.auth_mobile_code(user, password);

if (auth_token == None):
    print("ERREUR, nom d'utilisateur/mdp invalide?");
    sys.exit(-1);

with open("auth_token","w") as f:
    f.write(auth_token);

print("OK, token sauvegardé!");
