from random import randint;
from datetime import datetime;
import discord;
import api;
import json;
import os;
import sys;
import re;
import html;
import threading;
import asyncio;

REGEX_CLEAN = re.compile('<.*?>');
GLOBAL_CONFIG = {};
CACHE = {"news": [], "work": []};

with open('config.json', 'r') as f:
    GLOBAL_CONFIG = json.load(f);

auth_token = GLOBAL_CONFIG["mbn_token"];
etab_id = "";

try:
    with open('cache.json') as cache_file:
        CACHE = json.load(cache_file);
except FileNotFoundError:
    pass;


def cleanhtml(raw_html):
    cleantext = re.sub(REGEX_CLEAN, '', raw_html.replace("</p>", "\n"));
    return html.unescape(cleantext);

def restart_program():
    python = sys.executable;
    os.execl(python, python, *sys.argv);

def save_cache():
    with open('cache.json', 'w') as cache_file:
        json.dump(CACHE, cache_file);

async def display_embed(channel, title, content, color, timestamp, url, auteur):
    text = cleanhtml(content);
    arr = re.findall('.{1,2040}', text, re.DOTALL);
    if (len(arr) == 0 or len(arr) == 1):
        embed = discord.Embed(
            title = cleanhtml(title),
            description = "Aucun contenu." if len(arr) == 0 else arr[0],
            color = color,
            timestamp = timestamp,
            url = cleanhtml(url)
        );
        embed.set_footer(text = cleanhtml(auteur));
        await client.get_channel(channel).send("\n\n", embed = embed);
    else:
        for i, j in enumerate(arr, 1):
            if (i == 1):
                embed = discord.Embed(
                    title = cleanhtml(title),
                    description = j,
                    color = color,
                    url = cleanhtml(url)
                );
                await client.get_channel(channel).send("\n\n", embed = embed);
            elif (i == len(arr)):
                embed = discord.Embed(
                    description = j,
                    color = color,
                    timestamp = timestamp
                );
                embed.set_footer(text = cleanhtml(auteur));
                await client.get_channel(channel).send(embed = embed);
            else:
                embed = discord.Embed(
                    description = j,
                    color = color
                );
                await client.get_channel(channel).send(embed = embed);

async def load_news():
    news_list = api.get_news_list(auth_token, etab_id);
    # await client.get_channel(GLOBAL_CONFIG["channels"]["logs"]).send("Loading " + str(len(news_list)) + " news...");
    for news in reversed(news_list):
        if (news["uid"] in CACHE["news"]):
            continue;
        else:
            CACHE["news"].append(news["uid"]);
        
        news_content = api.get_news_content(auth_token, news["uid"]);
        await display_embed(
            GLOBAL_CONFIG["channels"]["news"],
            news_content["titre"], news_content["codeHTML"],
            randint(0, 0xFFFFFF),
            datetime.fromtimestamp(news_content["date"]/1000.0),
            news_content["url"],
            "Par " + news_content["auteur"]
        );
    save_cache();

async def load_work():
    work_list = api.get_work_list(auth_token, etab_id);
    # await client.get_channel(GLOBAL_CONFIG["channels"]["logs"]).send("Loading work...");
    for work_days in work_list["listeTravaux"]:
        date_todo = datetime.fromtimestamp(work_days["date"]/1000.0);
        for works in work_days["listTravail"]:
            date_from = datetime.fromtimestamp(works["date"]/1000.0);
            
            if (works["uidSeance"] + "-" + works["uid"] in CACHE["work"]):
                continue;
            else:
                CACHE["work"].append(works["uidSeance"] + "-" + works["uid"]);
            
            work_content = api.get_work_content(auth_token, etab_id, works["uidSeance"], works["uid"]);
            
            await display_embed(
                GLOBAL_CONFIG["channels"]["work"],
                date_todo.strftime("%d/%m/%Y") + " - " + works["matiere"],
                work_content["codeHTML"],
                randint(0, 0xFFFFFF),
                date_from,
                work_content["url"],
                ""
            );
    save_cache();

async def reload_data():
    await client.change_presence(
        activity = discord.Activity(
            name = "recharger...",
            type = discord.ActivityType.playing
        ),
        status=discord.Status.online
    );
    
    await load_news();
    await load_work();
    
    await client.change_presence(
        activity = discord.Activity(
            name = etab["nom"],
            type = discord.ActivityType.watching
        ),
        status=discord.Status.idle
    );

async def reload_infinite():
    while True:
        await reload_data();
        await asyncio.sleep(GLOBAL_CONFIG["reload_delay"]);

f_stop = threading.Event();


client = discord.Client();

@client.event
async def on_ready():
    await client.get_channel(GLOBAL_CONFIG["channels"]["logs"]).send("Logged!");
    # await client.get_channel(GLOBAL_CONFIG["channels"]["logs"]).send("Connecté à MBN: " + user_info["nom"] + " - " + etab["nom"]);
    await client.change_presence(
        activity = discord.Activity(
            name = etab["nom"],
            type = discord.ActivityType.watching
        ),
        status=discord.Status.idle
    );
    task = asyncio.create_task(reload_infinite());

@client.event
async def on_message(message):
    if message.author.id != GLOBAL_CONFIG["admin"]:
        return;
    
    if message.content.startswith('!clear'):
        await message.channel.delete_messages(await message.channel.history(limit=100).flatten());
    elif message.content.startswith("!reboot"):
        await client.get_channel(GLOBAL_CONFIG["channels"]["logs"]).send("Reboot...");
        restart_program();
    elif message.content.startswith("!ping"):
        await client.get_channel(GLOBAL_CONFIG["channels"]["logs"]).send("PONG BLYAT!");
    elif message.content.startswith("!reload"):
        await reload_data();
    elif message.content.startswith("!addwork"):
        args = message.content.split("; ");
        if (len(args) != 4):
            await client.get_channel(GLOBAL_CONFIG["channels"]["logs"]).send("!! Syntaxe: !addwork; pour le (dd/mm/YYYY); matiere; contenu");
            return;
        print(args);
        date = None;
        try:
            date = datetime.strptime(args[1], "%d/%m/%Y");
        except ValueError:
            await client.get_channel(GLOBAL_CONFIG["channels"]["logs"]).send("!! Syntaxe: !addwork; pour le (dd/mm/YYYY); matiere; contenu");
            return;
        
        await display_embed(
            GLOBAL_CONFIG["channels"]["work"],
            date.strftime("%d/%m/%Y") + " - " + args[2],
            args[3],
            randint(0, 0xFFFFFF),
            datetime.now(),
            "",
            "Ajouté manuellement."
        );
        
        print();
        
    elif message.content.startswith("!poweroff"):
        await client.get_channel(GLOBAL_CONFIG["channels"]["logs"]).send("Poweroff...");
        f_stop.set();
        sys.exit(0);



if (not api.validate_token(auth_token)):
    print("Token MBN invalide! Poweroff...");
    sys.exit(0);
user_info = api.get_user_info(auth_token);
etab_id = user_info["idEtablissementSelectionne"];
etab = user_info["etabs"][0];

client.run(GLOBAL_CONFIG["bot_token"]);
