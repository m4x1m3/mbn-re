import requests;
import time;
import json;

KDECOLE_VERSION = "3.4.14";

def __millis():
    return str(int(round(time.time() * 1000)));


def auth_mobile_code(user, mobile_code):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/activation/" + user + "/" + mobile_code + "/?_=" + __millis();

    r = requests.get(url, headers={
        "X-Kdecole-Auth": "null",
        "X-Kdecole-Vers": KDECOLE_VERSION
    });

    data = json.loads(r.text);

    if (data["success"]):
        return data["authtoken"];
    else:
        return None;

def validate_token(auth_token):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/starting/?_=" + __millis();

    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        data = json.loads(r.text);
        return False;
    else:
        return True;

def get_user_info(auth_token):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/infoutilisateur/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": "0",
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);

def check_mails(auth_token):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/messagerie/info/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        data = json.loads(r.text);
        return data["nbMessagesNonLus"];

def get_news_list(auth_token, etab_id):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/actualites/idetablissement/" + str(etab_id) + "/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);

def get_news_content(auth_token, news_uid):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/contenuArticle/article/" + str(news_uid) + "/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);

def get_calendar_list(auth_token, etab_id):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/calendrier/idetablissement/" + str(etab_id) + "/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);

def get_work_list(auth_token, etab_id):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/travailAFaire/idetablissement/" + str(etab_id) + "/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);

def get_work_content(auth_token, etab_id, seance_id, work_id):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/contenuActivite/idetablissement/" + str(etab_id) + "/" + seance_id + "/" + work_id + "/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);

def get_absence_list(auth_token, etab_id):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/consulterAbsences/idetablissement/" + str(etab_id) + "/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);

def get_marks_list(auth_token, etab_id):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/consulterNotes/idetablissement/" + str(etab_id) + "/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);

def get_marks_list_expanded(auth_token, etab_id):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/consulterReleves/idetablissement/" + str(etab_id) + "/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);

def get_messages_list(auth_token):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/messagerie/boiteReception/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);

def get_messages_content(auth_token, message_id):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/messagerie/communication/" + str(message_id) + "/?_=" + __millis();
    
    r = requests.get(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);

def set_messages_read(auth_token, message_id):
    url = "https://mobilite.monbureaunumerique.fr/mobilite/messagerie/communication/lu/" + str(message_id) + "/"; # Oui, ici pas de timestamp. C'est pas moi qui suis inconsistent, c'est le site.
    
    r = requests.put(url, headers={
        "X-Kdecole-Auth": auth_token.rstrip(),
        "X-Kdecole-Date": __millis(),
        "X-Kdecole-Vers": KDECOLE_VERSION
    });
    
    if (r.status_code == 403):
        return None;
    else:
        return json.loads(r.text);





