import sys;
import api;
import re;
from datetime import datetime;

REGEX_CLEAN = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});');

def cleanhtml(raw_html):
    cleantext = re.sub(REGEX_CLEAN, '', raw_html.replace("</p>", "\n"));
    return cleantext;

auth_token = "";
etab_id = "";

try:
    with open("auth_token", "r") as f:
        auth_token = f.read();
        
        if (not api.validate_token(auth_token)):
            print("ERREUR, token invalide, veuillez vous reconnecter.");
            sys.exit(1);
except FileNotFoundError:
    print("ERREUR, veuillez vous connecter pour obtenir un token.");
    sys.exit(1);

user_info = api.get_user_info(auth_token);
etab_id = user_info["idEtablissementSelectionne"];
etab = user_info["etabs"][0];


print(user_info["nom"] + " - " + etab["nom"]);
print("Vous avez " + str(api.check_mails(auth_token)) + " mail(s) non-lu.");
"""
print("------");
print("NEWSPAPERZ: ");
print("------");
print();

news_list = api.get_news_list(auth_token, etab_id);

for news in news_list:
    news_content = api.get_news_content(auth_token, news["uid"]);
    print(news_content["titre"]);
    print("--");
    print(cleanhtml(news_content["codeHTML"]));
"""
print("------");
print("ARBEIT: ");
print("------");
print();

work_list = api.get_work_list(auth_token, etab_id);

for work_days in work_list["listeTravaux"]:
    date_todo = datetime.fromtimestamp(work_days["date"]/1000.0);
    for works in work_days["listTravail"]:
        print(date_todo.strftime("%d/%m/%Y"));
        date_from = datetime.fromtimestamp(works["date"]/1000.0);
        print(date_from.strftime("%d/%m/%Y") + " - " + works["matiere"]);
        work_content = api.get_work_content(auth_token, etab_id, works["uidSeance"], works["uid"]);
        
        print(cleanhtml(work_content["codeHTML"]));
    
    # print(work_days);





